"""This is a simples Flask app
"""
from flask import Flask
app = Flask(__name__) # pylint: disable=invalid-name

@app.route('/')
def hello_world():
    """This function returns a string"""
    return 'Hell awaits!'

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
